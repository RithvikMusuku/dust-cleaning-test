# dust-cleaning-test

## Instructions for dust.py

1. Install Python 3. Install OpenCV. You can install OpenCV using the pip command below.

`pip install opencv-python`

2. Save `dust.py` to the directory containing a video of your test, clean reference video, and dust-covered reference video. These videos should have all lens distortion removed and should be cropped so that only the panel that is being tested is in the video. All three videos should have the same resolution.

3. Run dust.py using the below command line arguments.
    - The first three arguments are the names of the test video, clean reference video, and dust-covered reference video, respectively.
    - The four numbers are bounds for the section of the videos that you would like to analyze. 
      - The first two numbers are the starting and ending x coordinates that you would like to use, respectively. The last two numbers are the starting and ending y coordinates that you would like to use, respectively. 
      - An x coordinate of 0 represents the leftmost part of the video and an x coordinate of 1 represents the rightmost part of the video. A y coordinate of 0 represents the topmost part of the video and a y coordinate of 1 represents the bottommost part of the video. 
      - If you would like to analyze the entire video, you can use the bounds `0 1 0 1`. 
      - If you would like to analyze specific sections of the video, you need to provide the correct bounds. For example, if you would like to analyze the top right quadrant of the video, you can use the bounds `0.5 1 0 0.5`. 
    - The last argument below is the CSV file where you want to write the cleanliness data.

`python dust.py test_video.mp4 clean_reference.mp4 dusty_reference.mp4 0.2 0.5 0.4 0.9 data.csv`

4. The cleanliness data will automatically be saved to the CSV file name that you provided. The first column will be time (in seconds) and the second column will be cleanliness (on a scale of 0 to 1).

Note: It is possible to use any video file format that is supported by OpenCV.

## Instructions for all_in_one.py

1. Install Python 3. Install OpenCV. You can install OpenCV using the pip command below.

`pip install opencv-python`

2. Save `all_in_one.py` and `calibration_data.npz` to the directory containing a video of your test, clean reference video, and dust-covered reference video. These three videos should be original wide-angle videos from the GoPro Hero 3. The program will fix the lens distortion and crop the videos.

3. Run all_in_one.py using the below command line arguments.
    - The first three arguments are the names of the test video, clean reference video, and dust-covered reference video, respectively.
    - The last argument is the CSV file where you want to write the cleanliness data.

    `python all_in_one.py test_video.mp4 clean_reference.mp4 dusty_reference.mp4 data.csv`

4. The first frame of the clean reference video (with lens distortion removed) will pop up in a new window. Click on this window and drag your cursor to identify a rectangular region of interest. Press the SPACE or ENTER button and close the window. The program will automatically crop the clean reference video, dust-covered reference video, and test video to remove everything outside of the rectangular region of interest that you identified.

5. The cleanliness data will automatically be saved to the CSV file name that you provided. The first column will be time (in seconds) and the second column will be cleanliness (on a scale of 0 to 1).
