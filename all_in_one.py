import numpy as np
import cv2
import csv
import sys
import os

def extract_calibration_parameters(filename):
    calibration_file = np.load(filename)

    distortion_coefficients = calibration_file['distCoeff']
    intrinsic_matrix = calibration_file['intrinsic_matrix']

    calibration_file.close()

    return intrinsic_matrix, distortion_coefficients

def return_crop_bounds(filename, intrinsic_matrix, distortion_coefficients, crop_factor=0):
    video = cv2.VideoCapture(filename)
    width = video.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)
    size = (int(width), int(height))
    ret, frame = video.read()

    newMat, ROI = cv2.getOptimalNewCameraMatrix(intrinsic_matrix, distortion_coefficients, size, alpha = crop_factor, centerPrincipalPoint = 1)
    mapx, mapy = cv2.initUndistortRectifyMap(intrinsic_matrix, distortion_coefficients, None, newMat, size, m1type = cv2.CV_32FC1)
    video.release()
    dst = cv2.remap(frame, mapx, mapy, cv2.INTER_LINEAR)
    return cv2.selectROI(dst)

def return_average_frame(filename, crop_bounds, intrinsic_matrix, distortion_coefficients, crop_factor=0):
    video=cv2.VideoCapture(filename)
    width = video.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)
    size = (int(width), int(height))
    y_start = int(crop_bounds[1])
    y_end = int(crop_bounds[1]+crop_bounds[3])
    x_start = int(crop_bounds[0])
    x_end = int(crop_bounds[0]+crop_bounds[2])
    all_frames = np.zeros((y_end-y_start,x_end-x_start))
    while video.isOpened():
        ret,frame=video.read()
        newMat, ROI = cv2.getOptimalNewCameraMatrix(intrinsic_matrix, distortion_coefficients, size, alpha = crop_factor, centerPrincipalPoint = 1)
        mapx, mapy = cv2.initUndistortRectifyMap(intrinsic_matrix, distortion_coefficients, None, newMat, size, m1type = cv2.CV_32FC1)
        if ret:
            undistorted_frame = cv2.remap(frame, mapx, mapy, cv2.INTER_LINEAR)
            all_frames += cv2.cvtColor(undistorted_frame[y_start:y_end,x_start:x_end], cv2.COLOR_BGR2GRAY).astype(float)
        else:
            break
    video_length = video.get(cv2.CAP_PROP_FRAME_COUNT)
    video.release()
    return all_frames/video_length

def compare_sample_to_reference(filename, ref_clean, ref_dusty, write_file, crop_bounds, intrinsic_matrix, distortion_coefficients, crop_factor=0):
    csvfile = open(write_file, 'w', newline='')
    writer = csv.writer(csvfile, delimiter=',')

    video=cv2.VideoCapture(filename)
    framerate = video.get(5)

    width = video.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)
    size = (int(width), int(height))

    y_start = int(crop_bounds[1])
    y_end = int(crop_bounds[1]+crop_bounds[3])
    x_start = int(crop_bounds[0])
    x_end = int(crop_bounds[0]+crop_bounds[2])

    i = 0
    while video.isOpened():
        ret,frame=video.read()
        newMat, ROI = cv2.getOptimalNewCameraMatrix(intrinsic_matrix, distortion_coefficients, size, alpha = crop_factor, centerPrincipalPoint = 1)
        mapx, mapy = cv2.initUndistortRectifyMap(intrinsic_matrix, distortion_coefficients, None, newMat, size, m1type = cv2.CV_32FC1)
        if ret:
            undistorted_frame = cv2.remap(frame, mapx, mapy, cv2.INTER_LINEAR)
            gray_cropped_frame = (cv2.cvtColor(undistorted_frame[y_start:y_end,x_start:x_end], cv2.COLOR_BGR2GRAY)).astype(float)
            cleanliness_frame = (gray_cropped_frame - ref_dusty)/(ref_clean - ref_dusty)
            writer.writerow([i/framerate, np.average(cleanliness_frame)])
            i+=1
        else:
            break
    video.release()
    csvfile.close()

def main():
    program_absolute_dirpath = os.path.abspath(os.path.dirname(__file__))

    dusty_filepath = os.path.join(program_absolute_dirpath, sys.argv[3])
    clean_filepath = os.path.join(program_absolute_dirpath, sys.argv[2])
    sample_filepath = os.path.join(program_absolute_dirpath, sys.argv[1])
    data_filepath = os.path.join(program_absolute_dirpath, sys.argv[4])
    calibration_data_filepath = os.path.join(program_absolute_dirpath, 'calibration_data.npz')

    dusty_video = cv2.VideoCapture(dusty_filepath)
    clean_video = cv2.VideoCapture(clean_filepath)
    sample_video = cv2.VideoCapture(sample_filepath)

    if not (dusty_video.get(4)==clean_video.get(4)==sample_video.get(4) and dusty_video.get(3)==clean_video.get(3)==sample_video.get(3)):
        sys.exit('Video resolutions do not match')

    dusty_video.release()
    clean_video.release()
    sample_video.release()
    

    intrinsic_matrix, distortion_coefficients = extract_calibration_parameters(calibration_data_filepath)
    crop_bounds = return_crop_bounds(clean_filepath, intrinsic_matrix, distortion_coefficients)
    ref_dusty = return_average_frame(dusty_filepath, crop_bounds, intrinsic_matrix, distortion_coefficients)
    ref_clean = return_average_frame(clean_filepath, crop_bounds, intrinsic_matrix, distortion_coefficients)

    compare_sample_to_reference(sample_filepath, ref_clean, ref_dusty, data_filepath, crop_bounds, intrinsic_matrix, distortion_coefficients)

main()