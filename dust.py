import cv2
import numpy as np
import csv
import sys

def find_cleanliness(frame, ref_clean, ref_dusty):
    return (frame - ref_dusty)/(ref_clean - ref_dusty)

def average_clean(frame):
    return np.average(frame)

def compare_sample_to_reference(filename, ref_clean, ref_dusty, write_file, bounds):
    csvfile = open(write_file, 'w', newline='')
    writer = csv.writer(csvfile, delimiter=',')

    video=cv2.VideoCapture(filename)
    framerate = video.get(5)

    y_start = int(bounds[2] * video.get(4))
    y_end = int(bounds[3] * video.get(4))
    x_start = int(bounds[0] * video.get(3))
    x_end = int(bounds[1] * video.get(3))

    i = 0
    while video.isOpened():
        ret,frame=video.read()
        if ret:
            gray_frame = (cv2.cvtColor(frame[y_start:y_end,x_start:x_end], cv2.COLOR_BGR2GRAY)).astype(float)
            writer.writerow([i/framerate,average_clean(find_cleanliness(gray_frame, ref_clean, ref_dusty))])
            i+=1
        else:
            break
    video.release()
    csvfile.close()

def return_average_frame(filename, bounds):
    video=cv2.VideoCapture(filename)
    y_start = int(bounds[2] * video.get(4))
    y_end = int(bounds[3] * video.get(4))
    x_start = int(bounds[0] * video.get(3))
    x_end = int(bounds[1] * video.get(3))
    all_frames = np.zeros((y_end-y_start,x_end-x_start))
    while video.isOpened():
        ret,frame=video.read()
        if ret:
            all_frames += cv2.cvtColor(frame[y_start:y_end,x_start:x_end], cv2.COLOR_BGR2GRAY).astype(float)
        else:
            break
    video_length = video.get(cv2.CAP_PROP_FRAME_COUNT)
    video.release()
    return all_frames/video_length

def main():
    bounds = [float(i) for i in sys.argv[4:8]]

    if any(bound > 1 or bound < 0 for bound in bounds):
        sys.exit("Bounds must be between 0 and 1, inclusive")

    dusty_video = cv2.VideoCapture(sys.argv[3])
    clean_video = cv2.VideoCapture(sys.argv[2])
    sample_video = cv2.VideoCapture(sys.argv[1])

    if not (dusty_video.get(4)==clean_video.get(4)==sample_video.get(4) and dusty_video.get(3)==clean_video.get(3)==sample_video.get(3)):
        sys.exit('Video resolutions do not match')

    dusty_video.release()
    clean_video.release()
    sample_video.release()
    
    ref_dusty = return_average_frame(sys.argv[3], bounds)

    ref_clean = return_average_frame(sys.argv[2], bounds)

    compare_sample_to_reference(sys.argv[1], ref_clean, ref_dusty, sys.argv[8], bounds)

main()
